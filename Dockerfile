FROM akursar/ventrilo

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

ADD src/ventrilo_srv.ini.tmpl /opt/ventsrv/ventrilo_srv.ini
ADD src/ventrilo_ini_update /etc/my_init.d/ventrilo_ini_update

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


